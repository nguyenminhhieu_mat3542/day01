<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <style>
        .form-container {
            width: 40%;
            margin-left: auto;
            margin-right: auto;
            padding: 20px;
            border: 2px solid #648bae;
        }

        .register-form{
            width: 80%;
            margin-left: auto;
            margin-right: auto;
        }

        .register-form {
            margin-top: 15px;
            display: flex;
            align-items: center;
            
        }

        .label {
            margin-bottom: 10px;
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #5b9bd5;
            border: 2px solid #648bae;

        }
        .form-label{
            padding: 3% 5%;
            background-color: #5b9bd5;
            color: white;
            min-width: 15%;
            margin: 0 10px;
        }

        select{
            width: 45%;
            padding: 14px;
            border: 2px solid #648bae;
            box-sizing: border-box;
            margin-right: auto;
            margin-right: 70px
        }

        input[type="text"] {
            width: 80%;
            padding: 14px;
            border: 2px solid #648bae;
            box-sizing: border-box;

        }
        

        select {
            border-radius: 1px;
        }

        button {
            margin: 5% auto 0 auto;
            display: block;
            font-family: inherit;
            width: 25%;
            padding: 15px;
            border: 2px solid #648bae;
            background-color: #4CAF50;
            color: white;
            border-radius: 10px;
            font-size: 1rem;
        }
      
    </style>
</head>
<body>
    <div class="form-container">
            <div class="register-form">
                <label for="full-name" class="form-label">Họ và tên</label>
                <input type="text" id="full-name" name="full-name" placeholder="">
            </div>
            <div class="register-form">
                <label class="form-label">Giới tính</label>
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ');
                    for ($i = 0; $i < count($genders); $i++) {
                        $gender = $genders[$i];
                        echo '<input type="radio" id="gender-' . $i . '" name="gender" value="' . $gender . '">';
                        echo '<label for="gender-' . $i . '">' . $gender . '</label>';
                    }
                    ?>
            </div>
            <div class="register-form" >
                <label for="TenNganh" class="form-label">Phân khoa</label>
                <select id="TenNganh" name="Tennganh">
                    <option value="">--Chọn phân khoa--</option>
                    <?php
                    $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                    foreach ($departments as $key => $value) {
                        echo '<option value="' . $key . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </div>
                <button type="submit">Đăng ký</button>  
        </form>
    </div>
</body>
</html>
