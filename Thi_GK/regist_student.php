<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận</title>
    <style>
        .form-container {
            width: 40%;
            margin-left: auto;
            margin-right: auto;
            padding: 20px;
            border: 2px solid #648bae;
        }

        .register-form {
            width: 80%;
            margin-left: auto;
            margin-right: auto;
        }

        .register-form {
            margin-top: 10px;
            display: flex;
            align-items: center;
        }

        .label {
            margin-bottom: 10px;
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #5b9bd5;
            border: 2px solid #648bae;
        }

        .form-label {
            padding: 3% 5%;
            border: 2px solid #648bae;
            background-color: #4CAF50;
            color: white;
            min-width: 16%;
            margin: 0 10px;
        }

        select {
            width: 45%;
            padding: 16px;
            border: 2px solid #648bae;
            box-sizing: border-box;
            margin-right: auto;
            margin-right: 70px;
        }

        button {
            margin: 5% auto 0 auto;
            display: block;
            font-family: inherit;
            width: 25%;
            padding: 15px;
            border: 2px solid #648bae;
            background-color: #4CAF50;
            color: white;
            border-radius: 10px;
            font-size: 1rem;
        }

        .required {
            color: red;
        }

        .register-form input[type="text"] {
            border: none;
        }

        .register-form input[type="text"]:focus {
            outline: none;
        }
    </style>
</head>
<body>
<div class="form-container">
<?php
            if ($_SERVER["REQUEST_METHOD"] === "POST") {
                $fullName = $_POST["full-name"];
                $gender = $_POST["gender"];
                $dateOfBirth = $_POST["date-of-birth"];
                $diaChi = $_POST["DiaChi"];
                $city = $_POST["city"];
                $district = $_POST["district"];
            }

            echo "<div class='register-form'>";
            echo "<label for='full-name' class='form-label'>Họ và tên:</label>";
            echo "<input type='text' id='full-name' name='full-name' value='$fullName' readonly>";
            echo "</div>";

            echo "<div class='register-form'>";
            echo "<label for='gender' class='form-label'>Giới tính:</label>";
            echo "<input type='text' id='gender' name='gender' value='$gender' readonly>";
            echo "</div>";

            echo "<div class='register-form'>";
            echo "<label for='date-of-birth' class='form-label'>Ngày sinh:</label>";
            echo "<input type='text' id='date-of-birth' name='date-of-birth' value='$dateOfBirth' readonly>";
            echo "</div>";

            echo "<div class='register-form'>";
            echo "<label for='DiaChi' class='form-label'>Địa chỉ (Thành phố, Quận):</label>";
            echo "<input type='text' id='DiaChi' name='DiaChi' value='$city, $district' readonly>";
            echo "</div>";

            echo "<div class='register-form'>";
            echo "<label for='DiaChi' class='form-label'>Thông tin khác:</label>";
            echo "<input type='text' id='DiaChi' name='DiaChi' value='$diaChi' readonly>";
            echo "</div>";
            ?>
</div>
</body>
</html>
