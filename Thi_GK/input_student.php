<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <style>
        .form-container {
            width: 40%;
            margin-left: auto;
            margin-right: auto;
            padding: 20px;
            border: 2px solid #648bae;
        }

        .register-form{
            width: 80%;
            margin-left: auto;
            margin-right: auto;
        }

        .register-form {
            margin-top: 15px;
            display: flex;
            align-items: center;
        }

        .label {
            margin-bottom: 10px;
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #5b9bd5;
            border: 2px solid #648bae;
        }

        .form-label{
            padding: 3% 5%;
            border: 2px solid #648bae;
            background-color: #4CAF50;
            color: white;
            min-width: 16%;
            margin: 0 10px;
        }

        select{
            width: 45%;
            padding: 16px;
            border: 2px solid #648bae;
            box-sizing: border-box;
            margin-right: auto;
            margin-right: 70px
        }

        input[type="text"]#full-name {
            width: 80%;
            padding: 16px;
            border: 2px solid #648bae;
            box-sizing: border-box;
        }

        input[type="text"]#date-of-birth {
            width: 45%; 
            padding: 16px;
            border: 2px solid #648bae;
            box-sizing: border-box;
        }

        input[type="text"]#DiaChi {
        width: 80%; 
        padding: 16px;
        border: 2px solid #648bae;
        box-sizing: border-box;
        margin-top: 1px;
        
        }

        input[type="file"] {
            display: none;
        }

        select {
            border-radius: 1px;
        }

        button {
            margin: 5% auto 0 auto;
            display: block;
            font-family: inherit;
            width: 25%;
            padding: 15px;
            border: 2px solid #648bae;
            background-color: #4CAF50;
            color: white;
            border-radius: 10px;
            font-size: 1rem;
        }
        .required {
            color: red;
        }
        
        .custom-file-upload {
            cursor: pointer;
            padding: 2px 10px;
            background-color: #f0f0f0;
            color: black;
            border: 1px solid #000000;
            border-radius: 3px;
            font-size: 1rem;
        }

        #selected-image {
            max-width: 25%;
            display: none;
        }

    </style>
</head>
<body>
    <div class="form-container">
    <h1 style="text-align: center; margin-top: 20px;">Form đăng ký sinh viên</h1>
        <div id="error-message" class="error-message" style="text-align: left; color: red; padding-left: 70px;"></div>
        <form id="registration-form" method="POST" action="regist_student.php">
            <div id="error-container">
            </div>
            <div class="register-form">
                <label for="full-name" class="form-label ">Họ và tên <span class="required"></span></label>
                <input type="text" id="full-name" name="full-name" placeholder="">
                <input type="hidden" name="full-name" id="hidden-full-name">
            </div>
            <div class="register-form">
                <label class="form-label">Giới tính <span class="required"></span></label>
                <input type="hidden" name="gender" id="hidden-gender">
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ');
                    for ($i = 0; $i < count($genders); $i++) {
                        $gender = $genders[$i];
                        echo '<input type="radio" id="gender-' . $i . '" name="gender" value="' . $gender . '">';
                        echo '<label for="gender-' . $i . '">' . $gender . '</label>';
                    }
                    ?>
            </div>
            <div class="register-form">
                <label for="date-of-birth" class="form-label">Ngày sinh <span class="required"></span></label>
                <select id="year" name="year">
                    <option value="">Năm</option>
                    <?php
                    $currentYear = date("Y");
                    for ($i = $currentYear + 15; $i >= 1983; $i--) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }
                    ?>
                </select>

                <select id="month" name="month">
                    <option value="">Tháng</option>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                        $month = str_pad($i, 2, '0', STR_PAD_LEFT); // Đảm bảo rằng mỗi tháng có 2 chữ số
                        echo '<option value="' . $month . '">' . $month . '</option>';
                    }
                    ?>
                </select>

                <select id="day" name="day">
                    <option value="">Ngày</option>
                    <?php
                    for ($i = 1; $i <= 31; $i++) {
                        $day = str_pad($i, 2, '0', STR_PAD_LEFT); // Đảm bảo rằng mỗi ngày có 2 chữ số
                        echo '<option value="' . $day . '">' . $day . '</option>';
                    }
                    ?>
                </select>
                <input type="hidden" name="date-of-birth" id="hidden-date-of-birth">
            </div>

            <div class="register-form" style="align-items: flex-start;">
                <label for="city" class="form-label">Địa chỉ <span class="required"></span></label>
                <select id="city" name="city" onchange="loadDistricts()">
                    <option value="">Thành phố</option>
                    <option value="Hanoi">Hà Nội</option>
                    <option value="TP.HCM">TP.Hồ Chí Minh</option>
                </select>
                <select id="district" name="district">
                    <option value="">Quận--</option>
                </select>
            </div>

            <script>
                function loadDistricts() {
                    var citySelect = document.getElementById("city");
                    var districtSelect = document.getElementById("district");
                    districtSelect.innerHTML = "<option value=''>--Chọn Quận--</option>";

                    var selectedCity = citySelect.value;
                    if (selectedCity === "Hanoi") {
                        var hanoiDistricts = {
                            'HoangMai': 'Quận Hoàng Mai',
                            'ThanhTri': 'Quận Thanh Trì',
                            'NamTuLiem': 'Quận Nam Từ Liêm',
                            'HaDong': 'Quận Hà Đông',
                            'CauGiay': 'Quận Cầu Giấy'
                        };
                        for (var key in hanoiDistricts) {
                            districtSelect.innerHTML += '<option value="' + key + '">' + hanoiDistricts[key] + '</option>';
                        }
                    } else if (selectedCity === "TP.HCM") {
                        var tphcmDistricts = {
                            'District1': 'Quận 1',
                            'District2': 'Quận 2',
                            'District3': 'Quận 3',
                            'District7': 'Quận 7',
                            'District9': 'Quận 9'
                        };
                        for (var key in tphcmDistricts) {
                            districtSelect.innerHTML += '<option value="' + key + '">' + tphcmDistricts[key] + '</option>';
                        }
                    }
                }
            </script>
            <div class="register-form" style="align-items: flex-start;">
                <label for="Diachi" class="form-label"> Thông tin khác </label>
                <input type="text" id="DiaChi" name="DiaChi"placeholder="">
                <input type="hidden" name="dia-chi" id="hidden-dia-chi">
            </div>
            <button type="button" id="submit-button">Đăng ký</button> 
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#submit-button").click(function() {
                var fullName = $("#full-name").val();
                var genderSelected = $("input[name='gender']:checked").length > 0;
                var dateOfBirth = $("#day").val() + '/' + $("#month").val() + '/' + $("#year").val();
                var errorMessage = $("#error-message");

                errorMessage.html(""); 

                var errors = [];

                if (fullName.trim() === "") {
                    errors.push("Hãy nhập tên.");
                }

                if (!genderSelected) {
                    errors.push("Hãy chọn giới tính.");
                }

                if (dateOfBirth === "//") {
                    errors.push("Hãy nhập ngày sinh.");
                } else {               
                    var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;
                    if (!dateRegex.test(dateOfBirth)) {
                        errors.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                    }
                }

                var selectedCity = $("#city").val();
                var selectedDistrict = $("#district").val();
                if (selectedCity === "" || selectedDistrict === "") {
                    errors.push("Hãy chọn Thành phố và Quận.");
                }

                if (errors.length === 0) {
                    var tempForm = document.createElement("form");
                    tempForm.method = "POST";
                    tempForm.action = "regist_student.php";


                    var fullNameInput = document.createElement("input");
                    fullNameInput.type = "text";
                    fullNameInput.name = "full-name";
                    fullNameInput.value = fullName.trim();
                    tempForm.appendChild(fullNameInput);

                    var genderInput = document.createElement("input");
                    genderInput.type = "text";
                    genderInput.name = "gender";
                    genderInput.value = $("input[name='gender']:checked").val();
                    tempForm.appendChild(genderInput);

                    var dateOfBirthInput = document.createElement("input");
                    dateOfBirthInput.type = "text";
                    dateOfBirthInput.name = "date-of-birth";
                    dateOfBirthInput.value = dateOfBirth;
                    tempForm.appendChild(dateOfBirthInput);

                    var diaChiInput = document.createElement("input");
                    diaChiInput.type = "text";
                    diaChiInput.name = "dia-chi";
                    diaChiInput.value = $("#DiaChi").val();
                    tempForm.appendChild(diaChiInput);

                    document.body.appendChild(tempForm);
                    tempForm.submit();
                } else {
                    errorMessage.html(errors.join("<br>"));
                }
            });
        });
    </script>
</body>
</html>


