<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tìm kiếm danh sách sinh viên</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <style>
        #overlay {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
        }

        .popup {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: #fff;
            padding: 20px;
            border: 1px solid #ccc;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            z-index: 1000;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="search-box">
            <form method="post">
                <div class="add-btn">
                    <a href="../day05/register.php" class="add-button"><button>Thêm</button></a>
                </div>
            </form>
        </div>

        <p id="result-info"></p>
        <div id="student-table-container"></div>

        <div id="overlay"></div>
        <div id="confirm-popup" class="popup">
            <p>Bạn muốn xóa sinh viên này?</p>
            <button id="confirm-delete-button">Xóa</button>
            <button id="cancel-delete-button">Hủy</button>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $(".delete-button").click(function () {
                var studentId = $(this).data("student-id");
                $("#overlay").show();
                $("#confirm-popup").show();
                $("#confirm-delete-button").data("student-id", studentId);
            });

            $("#confirm-delete-button").click(function () {
                var studentId = $(this).data("student-id");
                deleteStudent(studentId);
                closePopup();
            });

            $("#cancel-delete-button").click(function () {
                closePopup();
            });

            function closePopup() {
                $("#overlay").hide();
                $("#confirm-popup").hide();
            }

            function deleteStudent(studentId) {
            }

            function displayStudents(students) {
                var tableHtml = "<table><tr><th>ID</th><th>Tên sinh viên</th><th>Khoa</th><th>Action</th></tr>";

                students.forEach(function (student) {
                    tableHtml += "<tr>";
                    tableHtml += "<td>" + student.id + "</td>";
                    tableHtml += "<td>" + student.full_name + "</td>";
                    tableHtml += "<td>" + student.department + "</td>";
                    tableHtml += "<td class='action'><button class='delete-button' data-student-id='" + student.id + "'>Xóa</button></td>";
                    tableHtml += "</tr>";
                });

                tableHtml += "</table>";
                $("#student-table-container").html(tableHtml);
            }
        });
    </script>
</body>

</html>
