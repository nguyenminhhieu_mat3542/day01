<?php

$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $studentId = $_GET['id'];

    $sql = "SELECT * FROM students WHERE id = :id";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':id', $studentId, PDO::PARAM_INT);
    $stmt->execute();

    if ($stmt->rowCount() > 0) {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        echo "<form method='post' action='process_update.php'>";
        echo "<input type='hidden' name='id' value='" . $row['id'] . "'>";
        echo "Tên sinh viên: <input type='text' name='full_name' value='" . $row['full_name'] . "'><br>";
        echo "Khoa: <input type='text' name='department' value='" . $row['department'] . "'><br>";
        echo "<input type='submit' value='Lưu'>";
        echo "</form>";
    } else {
        echo "Sinh viên không tồn tại.";
    }
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>
