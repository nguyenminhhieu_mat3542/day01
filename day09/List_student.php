<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tìm kiếm danh sách sinh viên</title>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <style>
        .container {
            width: 60%;
            margin: 2% auto 0 auto;
        }

        label {
            display: inline-block;
            width: 10%;
            padding: 5px;
        }

        .search-box {
            display: flex;
            flex-direction: column;
        }

        button {
            font-family: inherit;
            font-size: 1rem;
            color: white;
            width: 10%;
            padding: 10px;
            background-color: #4f81bd;
            border-radius: 6px;
            margin: 1% auto 1% auto;
            cursor: pointer;
        }

        .btn {
            text-align: center;
        }

        select,
        input[type="text"] {
            width: 20%;
            padding: 5px;
            margin-top: 5px;
        }
        .add-button {
            text-decoration: none;
            display: inline-flex;
            padding: 10px;
            background-color: #4f81bd;
            border-radius: 6px;
            margin-left: 800px;
            color: white;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        .search label,
        .search input[type="text"],
        .department label,
        .department select {
            margin-bottom: 0;
        }

        #resultCount {
            margin-top: 10px;
        }

        th,
        td {
            padding: 10px;
            text-align: center;
        }

        .action {
            border: 1px solid #6889b2;
            background-color: #92b1d6;
            padding: 3px;
            color: white;
        }

        .department,
        .search {
            display: flex;
            align-items: left;
            justify-content: center;
            margin-bottom: 10px;
            margin-left: 10px;
        }

        
    </style>
</head>

<body>
    <div class="container">
        <div class="search-box">
            <form method="post">
                <div class="department">
                    <label for="department">Khoa</label>
                    <select name="depts" id="getDept">
                        <option value="none" selected disabled hidden>--Chọn phân khoa--</option>
                        <option value="MAT">Khoa học máy tính</option>
                        <option value="KDL">Khoa học vật liệu</option>
                    </select>
                </div>
                <div class="search">
                    <label for="search">Từ khoá</label>
                    <input type="text" id="getName">
                </div>
                <div class="btn">
                    <button id="btn" type="submit" value="submit" name="search">Tìm kiếm</button>
                </div>
                <a href="../day06/register.php" class="add-button">Thêm</a>
            </form>
        </div>
        <p id="resultCount"></p>
        <p id="no"></p> 
        <table>
        </table>
    </div>
    <script>
        $(document).ready(function () {
            $("form").submit(function (e) {
                e.preventDefault();

                var department = $("#getDept").val();
                var keyword = $("#getName").val();

                $.ajax({
                    type: "POST",
                    url: "database.php",
                    data: {
                        depts: department,
                        search: keyword
                    },
                    success: function (response) {
                        var resultCount = (response.match(/<tr>/g) || []).length - 1;
                        $("#resultCount").text("Số sinh viên được tìm thấy: " + resultCount);
                        $("#no").html(response);
                    }
                });
            });
        });
    </script>

</body>

</html>
