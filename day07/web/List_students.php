<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Quản lý sinh viên</title>
  <style> 
       #container {
          width: 50%; 
          margin: auto; 
          padding: 20px; 
          border: 2px solid #648bae;
          box-sizing: border-box; 
        }
        #search-container {
          display: flex; 
          flex-direction: column; 
          align-items: center; 
      }
      .register-form input[type="text"],
      #inputDepartment {
          width: 45%; 
          padding: 10px; 
          margin-bottom: 10px; 
          box-sizing: border-box; 
          display: block;
      }
      #inputDepartment {
          width: 40%; 
      }
      #search-button {
          width: 10%; 
          padding: 10px; 
          margin-bottom: 10px; 
      }
      #resultMessage {
          margin-top: 10px;
      }
      #submit-button {
          margin-top: 20px;
          width: 100%; 
          padding: 15px; 
          border: 2px solid #648bae; 
          background-color: #4CAF50; 
          color: white; 
          border-radius: 10px; 
          font-size: 1rem; 
      }
      .required {
          color: red;
          margin-left: 5px; 
      }
  </style>
</head>
<body>
  <div id="search-container">
    <label for="inputDepartment">Khoa:</label>
    <select id="inputDepartment">
      <option value="MAT">Khoa học máy tính</option>
      <option value="KDL">Khoa học vật liệu</option>
    </select>
    <label for="inputKeyword">Từ khóa:</label>
    <input type="text" id="inputKeyword" placeholder="Nhập từ khóa">
    <button onclick="searchStudents()">Tìm kiếm</button>
    <p id="resultMessage">Số sinh viên được tìm thấy: <span id="resultCount">xxx</span></p>
  </div>

  <div id="student-list"></div>

  <script>
   function searchStudents() {
    var department = document.getElementById('inputDepartment').value;
    var keyword = document.getElementById('inputKeyword').value;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var students = JSON.parse(xhr.responseText);
            displayStudents(students);
        }
    };

    xhr.open('GET', 'get_students.php', true);
    xhr.send();
}

function displayStudents(students) {
    var studentListContainer = document.getElementById('student-list');
    studentListContainer.innerHTML = '';

    students.forEach(function (student) {
        var card = document.createElement('div');
        card.className = 'student-card';

        card.innerHTML = `
            <p><strong>ID:</strong> ${student.id}</p>
            <p><strong>Name:</strong> ${student.name}</p>
            <p><strong>Khoa:</strong> ${student.department}</p>
            <div class="action-buttons">
                <button onclick="editStudent(${student.id})">Sửa</button>
                <button onclick="deleteStudent(${student.id})">Xóa</button>
            </div>
        `;

        studentListContainer.appendChild(card);
    });

  
    document.getElementById('resultCount').innerText = students.length;
}
  </script>
</body>
</html>
