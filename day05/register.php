<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <style>
        .form-container {
            width: 40%;
            margin-left: auto;
            margin-right: auto;
            padding: 20px;
            border: 2px solid #648bae;
        }

        .register-form{
            width: 80%;
            margin-left: auto;
            margin-right: auto;
        }

        .register-form {
            margin-top: 15px;
            display: flex;
            align-items: center;
        }

        .label {
            margin-bottom: 10px;
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #5b9bd5;
            border: 2px solid #648bae;
        }

        .form-label{
            padding: 3% 5%;
            border: 2px solid #648bae;
            background-color: #4CAF50;
            color: white;
            min-width: 16%;
            margin: 0 10px;
        }

        select{
            width: 45%;
            padding: 16px;
            border: 2px solid #648bae;
            box-sizing: border-box;
            margin-right: auto;
            margin-right: 70px
        }

        input[type="text"]#full-name {
            width: 80%;
            padding: 16px;
            border: 2px solid #648bae;
            box-sizing: border-box;
        }

        input[type="text"]#date-of-birth {
            width: 45%; 
            padding: 16px;
            border: 2px solid #648bae;
            box-sizing: border-box;
        }

        input[type="text"]#DiaChi {
        width: 80%; 
        padding: 16px;
        border: 2px solid #648bae;
        box-sizing: border-box;
        margin-top: 1px;
        
        }

        input[type="file"] {
            display: none;
        }

        select {
            border-radius: 1px;
        }

        button {
            margin: 5% auto 0 auto;
            display: block;
            font-family: inherit;
            width: 25%;
            padding: 15px;
            border: 2px solid #648bae;
            background-color: #4CAF50;
            color: white;
            border-radius: 10px;
            font-size: 1rem;
        }
        .required {
            color: red;
        }
        
        .custom-file-upload {
            cursor: pointer;
            padding: 2px 10px;
            background-color: #f0f0f0;
            color: black;
            border: 1px solid #000000;
            border-radius: 3px;
            font-size: 1rem;
        }

        #selected-image {
            max-width: 25%;
            display: none;
        }

    </style>
</head>
<body>
    <div class="form-container">
        <div id="error-message" class="error-message" style="text-align: left; color: red; padding-left: 70px;"></div>
        <form id="registration-form" method="POST" action="confirm.php">
            <div id="error-container">
            </div>
            <div class="register-form">
                <label for="full-name" class="form-label ">Họ và tên <span class="required"> *</span></label>
                <input type="text" id="full-name" name="full-name" placeholder="">
                <input type="hidden" name="full-name" id="hidden-full-name">
            </div>
            <div class="register-form">
                <label class="form-label">Giới tính <span class="required"> *</span></label>
                <input type="hidden" name="gender" id="hidden-gender">
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ');
                    for ($i = 0; $i < count($genders); $i++) {
                        $gender = $genders[$i];
                        echo '<input type="radio" id="gender-' . $i . '" name="gender" value="' . $gender . '">';
                        echo '<label for="gender-' . $i . '">' . $gender . '</label>';
                    }
                    ?>
            </div>
            <div class="register-form" >
                <label for="TenNganh" class="form-label">Phân khoa <span class="required"> *</span></label>
                <input type="hidden" name="Tennganh" id="hidden-department">
                <select id="TenNganh" name="Tennganh">
                    <option value="">--Chọn phân khoa--</option>
                    <?php
                    $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                    foreach ($departments as $key => $value) {
                        echo '<option value="' . $key . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="register-form">
                <label for="date-of-birth" class="form-label"> Ngày sinh <span class="required">*</span></label>
                <input type="text" id="date-of-birth" name="date-of-birth" placeholder="dd/mm/yyyy" onblur="validateDate()"> 
                <input type="hidden" name="date-of-birth" id="hidden-date-of-birth">
            </div>
            <div class="register-form" style="align-items: flex-start;">
                <label for="Diachi" class="form-label"> Địa chỉ </label>
                <input type="text" id="DiaChi" name="DiaChi"placeholder="">
                <input type="hidden" name="dia-chi" id="hidden-dia-chi">
            </div>
            <div class="register-form">
                <label for="hinh-anh" class="form-label">Hình ảnh</label>
                <input type="file" id="hinh-anh" name="hinh-anh" style="display: none;">
                <input type="hidden" name="hinh-anh" id="hidden-hinh-anh">
                <label for="hinh-anh" class="custom-file-upload">Choose File</label><span id="file-name">No file chosen</span>
                <img id="selected-image" src="" alt="Selected Image" style="max-width: 25%; display: none;">
            </div>
            <script>
                const fileInput = document.getElementById("hinh-anh");
                const selectedImage = document.getElementById("selected-image");
                const customFileUpload = document.querySelector(".custom-file-upload");
                const fileNameSpan = document.getElementById("file-name");

                fileInput.addEventListener("change", function () {
                    if (fileInput.files.length > 0) {
                        const reader = new FileReader();
                        reader.onload = function (e) {
                            selectedImage.src = e.target.result;
                            selectedImage.style.display = "block"; 
                            customFileUpload.style.display = "none";
                            fileNameSpan.style.display = "none";
                            const label = fileInput.nextElementSibling; 
                            label.querySelector("span").textContent = fileInput.files[0].name; 
                        };
                        reader.readAsDataURL(fileInput.files[0]);
                    } else {
                        selectedImage.src = "";
                        selectedImage.style.display = "none";
                        const label = fileInput.nextElementSibling;
                        label.querySelector("span").textContent = "  No file chosen"; 
                    }
                });
            </script>
            <button type="button" id="submit-button">Đăng ký</button> 
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#submit-button").click(function() {
                var fullName = $("#full-name").val();
                var genderSelected = $("input[name='gender']:checked").length > 0;
                var department = $("#TenNganh").val();
                var dateOfBirth = $("#date-of-birth").val();
                var errorMessage = $("#error-message");

                errorMessage.html(""); 

                var errors = [];

                if (fullName.trim() === "") {
                    errors.push("Hãy nhập tên.");
                }

                if (!genderSelected) {
                    errors.push("Hãy chọn giới tính.");
                }

                if (department === "") {
                    errors.push("Hãy chọn phân khoa.");
                }

                if (dateOfBirth.trim() === "") {
                    errors.push("Hãy nhập ngày sinh.");
                } else {               
                    var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;
                    if (!dateRegex.test(dateOfBirth)) {
                        errors.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                    }
                }

                if (errors.length === 0) {
                    // Cập nhật các trường dữ liệu ẩn
                    $("#hidden-full-name").val(fullName);
                    $("#hidden-gender").val($("input[name='gender']:checked").val());
                    $("#hidden-department").val($("#TenNganh").val());
                    $("#hidden-date-of-birth").val(dateOfBirth);
                    $("#hidden-dia-chi").val($("#DiaChi").val());

                    // Cập nhật hình ảnh nếu có
                    const fileInput = document.getElementById("hinh-anh");
                    if (fileInput.files.length > 0) {
                        const file = fileInput.files[0];
                        const reader = new FileReader();
                        reader.onload = function (e) {
                            $("#hidden-hinh-anh").val(e.target.result);
                            // Tiến hành submit form
                            $("#registration-form").submit();
                        };
                        reader.readAsDataURL(file);
                    } else {
                        // Nếu không có hình ảnh, tiến hành submit form
                        $("#registration-form").submit();
                    }
                } else {
                    errorMessage.html(errors.join("<br>")); 
                }
            });
        });
    </script>
</body>
</html>
